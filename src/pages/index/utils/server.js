import axios from 'axios';
import qs from 'qs';
import md5 from 'md5';
const instance = axios.create({
	baseURL: '/'
});
instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
// Add a request interceptor
instance.interceptors.request.use(function(config) {
	var uid = localStorage.getItem('huankemao_user_id');
	var time = parseInt(new Date().getTime() / 1000);
	var corp_id = localStorage.getItem('huankemao_corp_id');
	config.data.corp_id = corp_id;
	var sign = md5(time + md5('huankemao' + config.url.substr(0, config.url.length) + qs
		.stringify(config.data)));
	var obj = {
		uid,
		time,
		sign
	}
	config.data = qs.stringify(config.data);
	config.headers['Auth'] = JSON.stringify(obj);
	// Do something before request is sent
	return config;
}, function(error) {
	// Do something with request error
	return Promise.reject(error);
});

// Add a response interceptor
instance.interceptors.response.use(function(response) {
	return response.data;
}, function(error) {
	// Do something with response error
	return Promise.reject(error);
});

export default instance;
