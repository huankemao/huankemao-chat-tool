import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
		path: '/',
		redirect: '/detail'
	},
	{
		path: '/detail',
		component: () => import('../views/Detail/index.vue')
	}, {
		path: '/detail_label',
		component: () => import('../views/Detail/label.vue')
	},
	{
		path: '/detail_follow',
		component: () => import('../views/Detail/follow.vue')
	},
	{
		path: '/detail_portrait',
		component: () => import('../views/Detail/portrait.vue')
	},
]

const router = new VueRouter({
	routes
})

export default router
