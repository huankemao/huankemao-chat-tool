import request from '../utils/server.js';

export const getCustomerInfo = (option) => request.post('/api.php/get_customer_info', option); // 获取客户详情

export const getFollowList = (option) => request.post('/api.php/get_customer_follow_list', option); // 获取客户跟进记录列表

export const getPortrait = (option) => request.post('/api.php/get_customer_portrait', option); // 获取客户画像

export const getTrackList = (option) => request.post('/api.php/get_customer_track_list', option); // 获取互动轨迹列表

export const getTagTree = (option) => request.post('/api.php/get_customer_tag_tree', option); // 获取客户标签树结构

export const fetchTagging = (option) => request.post('/api.php/customer_tagging', option); // 客户打标签/移除标签

export const fetchAddFollow = (option) => request.post('/api.php/add_customer_follow', option); // 添加客户跟进

export const getIndustryList = (option) => request.post('/api.php/show_edit_customer_portrait', option); // 获取行业列表

export const editPortrait = (option) => request.post('/api.php/edit_customer_info', option); //编辑客户画像
