import request from '../utils/server.js';

export const getUserInfo = (option) => request.post('/api.php/get_user_info', option); // 企业微信登录

export const getJsSdk = (option) => request.post('/api.php/get_js_sdk', option); //获取JSSDK

export const uploadUrl = '/api.php/upload_file_oss'; // 公共上传文件
