import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'normalize.css/normalize.css'
import {
	uploadImg,
	dateFormat,
	getQuery
} from './utils/index.js'
import ElementUI from 'element-ui' // 组件
import 'element-ui/lib/theme-chalk/index.css' // 样式

Vue.use(ElementUI) // 注册
Vue.config.productionTip = false;
Vue.prototype.$uploadImg = uploadImg;
Vue.prototype.$dateFormat = dateFormat;
Vue.prototype.$getQuery = getQuery;


new Vue({
	router,
	render: h => h(App)
}).$mount('#app')
