module.exports = {
	publicPath: '/chat-tool',
	pages: {
		index: { // 客户详情
			// page 的入口
			entry: 'src/pages/index/main.js',
			// 模板来源
			template: 'public/detail.html',
			// 在 dist/index.html 的输出
			filename: 'detail.html',
		},
		chat: {
			// page 的入口
			entry: 'src/pages/chat/main.js',
			// 模板来源
			template: 'public/index.html',
			// 在 dist/index.html 的输出
			filename: 'index.html',
		}
	}
}
